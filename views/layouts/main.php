<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="">
  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>

  <!-- <link rel="manifest" href="site.webmanifest"> -->
  <!-- Place favicon.ico in the root directory -->

  <!-- CSS here -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/magnific-popup.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/themify-icons.css">
  <link rel="stylesheet" href="css/nice-select.css">
  <link rel="stylesheet" href="css/flaticon.css">
  <link rel="stylesheet" href="css/gijgo.css">
  <link rel="stylesheet" href="css/animate.min.css">
  <link rel="stylesheet" href="css/slick.css">
  <link rel="stylesheet" href="css/slicknav.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php $this->beginBody() ?>
<!--<div class="wrap">-->
  <?php
  //    NavBar::begin([
  //        'brandLabel' => Yii::$app->name,
  //        'brandUrl' => Yii::$app->homeUrl,
  //        'options' => [
  //            'class' => 'navbar-inverse navbar-fixed-top',
  //        ],
  //    ]);
  //    echo Nav::widget([
  //        'options' => ['class' => 'navbar-nav navbar-right'],
  //        'items' => [
  //            ['label' => 'Home', 'url' => ['/site/index']],
  //            ['label' => 'About', 'url' => ['/site/about']],
  //            ['label' => 'Contact', 'url' => ['/site/contact']],
  //            Yii::$app->user->isGuest ? (
  //                ['label' => 'Login', 'url' => ['/site/login']]
  //            ) : (
  //                '<li>'
  //                . Html::beginForm(['/site/logout'], 'post')
  //                . Html::submitButton(
  //                    'Logout (' . Yii::$app->user->identity->username . ')',
  //                    ['class' => 'btn btn-link logout']
  //                )
  //                . Html::endForm()
  //                . '</li>'
  //            )
  //        ],
  //    ]);
  //    NavBar::end();
  ?>
  <?= $this->render('header') ?>
<!--  <div class="container">-->
    <!--    --><?php //Breadcrumbs::widget([
    //      'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    //    ]) ?>
    <?= Alert::widget() ?>
    <?= $content ?>
<!--  </div>-->
<!--</div>-->

<?= $this->render('footer') ?>

<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="js/vendor/jquery-1.12.4.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/ajax-form.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/scrollIt.js"></script>
<script src="js/jquery.scrollUp.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/nice-select.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/gijgo.min.js"></script>

<!--contact js-->
<script src="js/contact.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/mail-script.js"></script>

<script src="js/main.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
